exports.list = function(req, res){

 if (req.session.uid == null) {
    res.redirect('/login');
 }
 else
 {
    req.getConnection(function(err,connection){
       
        var query = connection.query('SELECT * FROM user WHERE UserCode = ? limit 1',[req.session.uid],function(err,rows)
        {
            
            if(err)
                console.log("Error Selecting : %s ",err );
              res.render('catalogues',{data:rows});      
         });
    });                 
    
 }
    
};
exports.getDataList = function(req,res){
					req.getConnection(function(err,connection){
       
        var query = connection.query('SELECT * FROM catalogue WHERE UserCode = ?  ORDER BY CatalogueCode DESC',[req.session.uid],function(err,rows)
        {
            
            if(err)
                console.log("Error Selecting : %s ",err );
           res.json(rows);          
         });
    });					
};
exports.save=function(req, res){
var temp=req.param('title');
var temp2=req.session.uid;
var data={

CatalogueName : temp,
UserCode : temp2
};
req.getConnection(function (err, connection) {
	connection.query('insert into catalogue set ?',data,function(err,rows){
	 if(err)
	                console.log("Error Selecting : %s ",err );
	});
	connection.query('SELECT * FROM catalogue WHERE UserCode = ?  ORDER BY CatalogueCode DESC ',[req.session.uid],function(err,rows)
        {
            
            if(err)
                console.log("Error Selecting : %s ",err );
           res.json(rows);          
         });
});
};