exports.list=function(req,res)
{
 if (req.session.uid == null) {
    res.redirect('/login');
 }
 else
 {
   req.getConnection(function(err,connection){
       
        var query = connection.query('SELECT * FROM user WHERE UserCode = ? limit 1',[req.session.uid],function(err,rows)
        {
            
            if(err)
                console.log("Error Selecting : %s ",err );
              res.render('tasks',{data:rows,CatalogueCode:req.param('id')});      
         });
    });          
 }
};
exports.getDataList=function(req,res)
{
	req.getConnection(function(err,connection){
       var temp=JSON.parse(JSON.stringify(req.param("catalogueCode")));
        var query = connection.query('SELECT * FROM task WHERE UserCode = ? AND CatalogueCode = ?  ORDER BY TaskCode DESC',[req.session.uid,temp],function(err,rows)
        {
            
            if(err)
                console.log("Error Selecting : %s ",err );
           res.json(rows);          
         });
    });		
  //  res.json(req.param("CatalogueCode"));	
};
exports.save=function(req, res){
var taskName=JSON.parse(JSON.stringify(req.param('taskName')));
var content=JSON.parse(JSON.stringify(req.param('content')));
var catalogueCode=JSON.parse(JSON.stringify(req.param("catalogueCode")));
var temp2=req.session.uid;
var data={

TaskName : taskName,
Content : content, 
CatalogueCode : catalogueCode,
UserCode : temp2
};
req.getConnection(function (err, connection) {
  connection.query('insert into task set ?',data,function(err,rows){
   if(err)
                  console.log("Error Selecting : %s ",err );
  });
  connection.query('SELECT * FROM task WHERE UserCode = ? AND CatalogueCode = ?  ORDER BY TaskCode DESC',[req.session.uid,catalogueCode],function(err,rows)
        {
            
            if(err)
                console.log("Error Selecting : %s ",err );
           res.json(rows);          
         });
});
};
exports.edit=function(req, res){
var taskName=JSON.parse(JSON.stringify(req.param('taskName')));
var content=JSON.parse(JSON.stringify(req.param('content')));
var catalogueCode=JSON.parse(JSON.stringify(req.param("catalogueCode")));
var taskCode=JSON.parse(JSON.stringify(req.param("taskID")));
var temp2=req.session.uid;
var data={

TaskName : taskName,
Content : content, 
CatalogueCode : catalogueCode,
UserCode : temp2
};
req.getConnection(function (err, connection) {
  connection.query('update task set ? WHERE UserCode = ? AND CatalogueCode = ? AND TaskCode = ?',[data,req.session.uid,catalogueCode,taskCode],function(err,rows){
   if(err)
                  console.log("Error Selecting : %s ",err );
  });
  connection.query('SELECT * FROM task WHERE UserCode = ? AND CatalogueCode = ?  ORDER BY TaskCode DESC',[req.session.uid,catalogueCode],function(err,rows)
        {
            
            if(err)
                console.log("Error Selecting : %s ",err );
           res.json(rows);          
         });
});
};
exports.delete=function(req, res){
var catalogueCode=JSON.parse(JSON.stringify(req.param("catalogueCode")));
var taskCode=JSON.parse(JSON.stringify(req.param("taskID")));
var temp2=req.session.uid;
req.getConnection(function (err, connection) {
  connection.query('delete from task WHERE UserCode = ? AND CatalogueCode = ? AND TaskCode = ?',[req.session.uid,catalogueCode,taskCode],function(err,rows){
   if(err)
                  console.log("Error Selecting : %s ",err );
  });
  connection.query('SELECT * FROM task WHERE UserCode = ? AND CatalogueCode = ?  ORDER BY TaskCode DESC',[req.session.uid,catalogueCode],function(err,rows)
        {
            
            if(err)
                console.log("Error Selecting : %s ",err );
           res.json(rows);          
         });
});
};