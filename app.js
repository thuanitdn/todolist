
/**
 * Module dependencies.
 */

var express = require('express');
var routes = require('./routes');
var http = require('http');
var path = require('path');
// express-session
var session = require('express-session');
//express-session
//load customers route
var catalogues = require('./routes/catalogues'); 
var users=require('./routes/users');
var tasks=require('./routes/tasks');
var app = express();

var connection  = require('express-myconnection'); 
var mysql = require('mysql');

// all environments
app.set('port', process.env.PORT || 4300);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
//app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.json());
app.use(express.urlencoded());
app.use(express.methodOverride());
// express-session
app.use(session({
  secret: '12345678qwe',
  resave: false,
  saveUninitialized: true
}));
//express -session
app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

/*------------------------------------------
    connection peer, register as middleware
    type koneksi : single,pool and request 
-------------------------------------------*/

app.use(
    
    connection(mysql,{
        
        host: 'localhost',
        user: 'root',
        password : '',
        port : 3306, //port mysql
        database:'nodejs'

    },'pool') //or single

);



app.get('/', routes.index);
app.get('/catalogues', catalogues.list);
app.get('/getDataCatalogues', catalogues.getDataList);
app.post('/insertDataCatalogues',catalogues.save);
app.get('/signup',users.signup);
app.get('/addUser',users.save);
app.get('/login',users.login);
app.post('/addSession',users.addSession);
app.get('/logout',users.deleteSession);
app.get('/tasks/:id',tasks.list);
app.post('/getDataTasks',tasks.getDataList);
app.post('/insertDataTasks',tasks.save);
app.post('/editDataTasks',tasks.edit);
app.post('/deleteDataTasks',tasks.delete);
app.use(app.router);

http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});
