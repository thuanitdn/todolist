-- phpMyAdmin SQL Dump
-- version 3.5.5
-- http://www.phpmyadmin.net
--
-- Client: sql5.freemysqlhosting.net
-- Généré le: Sam 02 Mai 2015 à 17:05
-- Version du serveur: 5.5.40-0ubuntu0.14.04.1
-- Version de PHP: 5.3.28

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `sql575726`
--

-- --------------------------------------------------------

--
-- Structure de la table `catalogue`
--

CREATE TABLE IF NOT EXISTS `catalogue` (
  `CatalogueCode` int(4) NOT NULL AUTO_INCREMENT,
  `CatalogueName` varchar(100) DEFAULT NULL,
  `UserCode` int(4) DEFAULT NULL,
  PRIMARY KEY (`CatalogueCode`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=67 ;

--
-- Contenu de la table `catalogue`
--

INSERT INTO `catalogue` (`CatalogueCode`, `CatalogueName`, `UserCode`) VALUES
(65, 'Lập trình node js -  angularjs', 25);

-- --------------------------------------------------------

--
-- Structure de la table `task`
--

CREATE TABLE IF NOT EXISTS `task` (
  `TaskCode` int(4) NOT NULL AUTO_INCREMENT,
  `TaskName` varchar(100) DEFAULT NULL,
  `Content` text,
  `CatalogueCode` int(4) DEFAULT NULL,
  `UserCode` int(4) DEFAULT NULL,
  PRIMARY KEY (`TaskCode`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=20 ;

--
-- Contenu de la table `task`
--

INSERT INTO `task` (`TaskCode`, `TaskName`, `Content`, `CatalogueCode`, `UserCode`) VALUES
(10, 'adda', 'adada', 63, 25),
(12, 'Day 1', 'Đọc tài liệu node js , angular js', 65, 25),
(13, 'Day 2', 'Thiết kế giao diện thiết kế database', 65, 25),
(14, 'Day 3', 'Coding', 65, 25),
(15, 'Day 4', 'Testing và cài đặt lên host', 65, 25),
(16, 'day 5', 'finish và nộp bài', 65, 25);

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `UserCode` int(4) NOT NULL AUTO_INCREMENT,
  `FirstName` varchar(30) DEFAULT NULL,
  `LastName` varchar(30) DEFAULT NULL,
  `Email` varchar(30) DEFAULT NULL,
  `Password` varchar(30) DEFAULT NULL,
  `SignUpDate` datetime DEFAULT NULL,
  PRIMARY KEY (`UserCode`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=27 ;

--
-- Contenu de la table `user`
--

INSERT INTO `user` (`UserCode`, `FirstName`, `LastName`, `Email`, `Password`, `SignUpDate`) VALUES
(25, 'thuan', 'le', 'vanthuan296@gmail.com', '123456', NULL),
(26, 'Thi lai', 'le', 'thilaidn011@gmail.com', '12345678', NULL);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
